#!/usr/bin/perl
use Data::Dumper;
=pod

Registers:
w x y z

Instructions:
mov reg,number
mov reg,reg
mov reg,[address]
mov [address],number
mov [address],reg
mov [reg],reg
mov reg,[reg]
mov [name],reg
...

cmp reg,reg
cmpg reg,reg
cmpl reg,reg

add reg,reg,reg
sub
and
or
xor
not reg,reg

nop

jmp label ;relative jumps
jmpif label ;relative jumps
jmplong reg|number ;jumpes to address

sf reg ; set flag
rf reg ; remember flag, (move to) register flag


Numbers:
hex 0xab, abh, 0ah
bin 1001010b
dec 109019

Labels & Names
[a-zA-Z_][a-zA-Z0-9_]

Comment:
;


Variable:
var name 	;allocate 1 
var name number ;allocate number


Ohters:
offset number ;set offset
offset +number ;adds to offset
offset -number ;subs from offset


=cut

sub trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };
sub n
{
    my ($n) = @_;
    local $_ = trim($n);
    
    if(/^([0-9a-fA-F]+)[Hh]$/){ return hex($1); }
    if(/^0x([0-9a-fA-F]+)$/){ return hex($1); }
    if(/^([01]+)b$/){ return oct("0b".$1);}
    if(/^[0-9]+$/){ return int($n); }
    
    
    print "error: wrong number format ($n)\n";
    exit(1);
}

sub isnumber
{
    my ($n) = @_;
    local $_ = trim($n);
    
    if(/^([0-9a-fA-F]+)[Hh]$/){ return 1; }
    if(/^0x([0-9a-fA-F]+)$/){ return 1; }
    if(/^([01]+)b$/){ return 1;}
    if(/^[0-9]+$/){ return 1; }
    
    return 0;

}


my @lines =();
my ($filein,$fileout) = @ARGV;

if( (defined $filein) and (defined $fileout) ){
    #reading from file
    open(f,"<", $filein) or die "Cannot open file '$file'\n";
    @lines = <f>;
    close f;
}else{
    print "error: wrong arguments\n";
    print "Usage: ./as.pl <file.asm> <file.bin>\n";
    exit(1);
}
    
    

my %label = ();
my %var = ();
my $addr = 0;

my $i = 1;
foreach (@lines){
    $_=trim($_);

    if(/\s*offset\s+(\+|-)?([^\s]+)\s*$/){
	if( $1 eq "+" ) { $addr += n($2); } 
	if( $1 eq "-" ) { $addr -= n($2); }
	if( $1 eq "" ) { $addr = n($2); }
    }
    
    if(/^\s*mov|cmp|cmpg|cmpl|add|sub|and|or|xor|not|nop|jmp|jmpif|jmplong/x) {
	$addr+=1;
    }
    
    if(/^\s*([a-zA-Z_][a-zA-Z_0-9]*):\s*$/){
	if( exists $label{$1} ) {
	    print "error: duplicate label '$1' in line $i\n";
	    exit(1);
	}
	$label{$1}=n($addr);
    }
    
    ++$i;
    
}

$addr=0;
$i=1;
foreach (@lines) {
    $_=trim($_);

    if(/\s*offset\s+(\+|-)?([^\s]+)\s*$/){
	if( $1 eq "+" ) { $addr += n($2); } 
	if( $1 eq "-" ) { $addr -= n($2); }
	if( $1 eq "" ) { $addr = n($2); }
    }
    
    if(/^\s*var\s+([a-zA-Z_][a-zA-Z_0-9]*)(:?\s+([^\s]+))?\s*$/){
	if( exists $var{$1} ) {
	    print "error: duplicate var '$1' in line $i\n";
	    exit(1);
	}
	if( $2 eq "" ) {
	    $var{$1} = $addr;
	    $addr+=1;
	}else{
	    my $n = n($2);
	    $var{$1} = $addr;
	    $addr+=$n;
	}
	
    }
    ++$i;
}

sub nop
{
    return 0;
}

sub jmp
{
    my ($addr,$n) = @_;
    my $diff = $n - $addr;
    if( $diff < 0 ) {
	return 0x9 + ((-$diff)<<16);
    }else{
	return 0x8 + (($diff)<<16);
    }
    
}
sub jmpif
{
    my ($addr,$n) = @_;
    my $diff = $n - $addr;
    if( $diff < 0 ) {
	return 0x10 + ((-$diff)<<16);
    }else{
	return 0xf + (($diff)<<16);
    }
    
}

sub jmplong
{
    my ($n) = @_;
    return 0xc + ($n<<16);
}

sub cmpegl
{
    my ($r1,$r2,$s) = @_;
    my $sel = 1;
    if( $s eq "g" ) { $sel = 0; }
    if( $s eq "l" ) { $sel = 2; }
    if( $s eq "e" ) { $sel = 1; }
    return 0xe + 0x100*$r1+0x400*$r2 + 0x1000*$sel;
}

sub alusel
{
    my ($cmd) = @_;
    if($cmd eq "add" ) { return 0; }
    if($cmd eq "sub" ) { return 1; }    
    if($cmd eq "and" ) { return 3; }
    if($cmd eq "or" ) { return 4; }
    if($cmd eq "xor" ) { return 5; }
    
    if($cmd eq "not" ) { return 2; }
    
    print "error: wrong alu command '$cmd'\n";
    exit(1);
}

sub regn
{
    my ($reg) = @_;
    #print "reg=$reg\n";
    if( $reg =~ /^[Xx]$/ ) { return 0; } 
    if( $reg =~ /^[Yy]$/ ) { return 1; }
    if( $reg =~ /^[Zz]$/ ) { return 2; }     
    if( $reg =~ /^[Ww]$/ ) { return 3; }     
    
    print "error: wrong register '$reg' in line $i\n";
    exit(1);
}

sub isreg
{
    my ($reg) = @_;
    if( $reg =~ /^[Xx]$/ ) { return 1; } 
    if( $reg =~ /^[Yy]$/ ) { return 1; }
    if( $reg =~ /^[Zz]$/ ) { return 1; }     
    if( $reg =~ /^[Ww]$/ ) { return 1; }     
    return 0;
}

sub isvar
{
    my ($v) = @_;
    if( exists $var{$v} ) { return 1;}else {return 0;}
}

$addr=0;
$i=1;
my %bin = ();
foreach (@lines){
    $_=trim($_);
    if(/\s*offset\s+(\+|-)?([^\s]+)\s*$/){
	if( $1 eq "+" ) { $addr += n($2); } 
	if( $1 eq "-" ) { $addr -= n($2); }
	if( $1 eq "" ) { $addr = n($2); }
	
    }
    
    if(/^\s*nop\s*$/){
	$bin{$addr} = nop();
	$addr+=1;
    }
    
    if(/^\s*jmp\s+([a-zA-Z_][a-zA-Z_0-9]*)\s*$/){
	if( exists $label{$1} ){
	    $bin{$addr} = jmp($addr,$label{$1});
	    $addr+=1;
	}else{
	    print "error: unknown label '$1' in line $i\n";
	    exit(1);
	}
    }
    if(/^\s*jmpif\s+([a-zA-Z_][a-zA-Z_0-9]*)\s*$/){
	if( exists $label{$1} ){
	    $bin{$addr} = jmpif($addr,$label{$1});
	    $addr+=1;
	}else{
	    print "error: unknown label '$1' in line $i\n";
	    exit(1);
	}
    }
    if(/^\s*jmplong\s+([^\s]+)\s*$/){
	if( exists $label{$1} ){
	    $bin{$addr} = jmplong($label{$1});
	    $addr+=1;
	}else{
	    my $n = n($1);
	    $bin{$addr} = jmplong($n);
	    $addr+=1;
	}
    }
    
    if(/^\s*cmp([egl])?\s+([^\s]+)\s+([^\s]+)\s*$/){
	my $r1 = regn($2);
	my $r2 = regn($3);
	#print "r = r1=$r1 r2=$r2\n";
	$bin{$addr} = cmpegl($r1,$r2,$1);
	$addr+=1;
    }
    
    if(/^\s*mov\s+([^\s]+)\s+([^\s]+)\s*$/){
	my $a1 = trim($1);
	my $a2 = trim($2);
	if($a1 =~ /^[xyzwXYZW]$/ and isnumber($a2) ){
	    # mov reg number
	    $bin{$addr} = 0x1 + (n($a2)<<16) + 0x40 * regn($a1);
	    $addr+=1;
	}elsif($a1 =~ /^[xyzwXYZW]$/ and $a2 =~ /^[xyzwXYZW]$/ ){
	    # mov reg reg
	    $bin{$addr} = 0x2 + regn($a2)*0x100 + regn($a1)*0x40;
	    $addr+=1;
	}elsif($a1=~/^[xyzwXYZW]$/ and $a2 =~ /^\s*\[\s*([^\s]+)\s*\]\s*$/){
	    # mov reg [var|number|reg]
	    my $v = $1;
	    if(exists $var{$v}){
		# move reg [var]
		$bin{$addr} = 0x4 + ($var{$v}<<16) + 0x40*regn($a1);
		$addr+=1;
	    }elsif(isreg($v)){
		# mov reg [reg]
		$bin{$addr} = 0x5 + 0x40*regn($a1) + 0x100*regn($v);
		$addr+=1;
	    }else{ 
		# move reg [number]
		$bin{$addr} = 0x4 + (n($v)<<16) + 0x40 *regn($a1);
		$addr+=1;
	    }
	}elsif($a2=~/^[xyzwXYZW]$/ and $a1 =~ /^\s*\[\s*([^\s]+)\s*\]\s*$/){
	    # mov [var|number|reg] reg
	    my $v = $1;
	    if( exists $var{$v} ){
		# mov [var] reg
		$bin{$addr} = 0x3 + 0x400*regn($a2) + ($var{$v}<<16);
		$addr+=1;
	    }elsif( isreg($v) ){
		# mov [reg] reg
		$bin{$addr} = 0x6 + 0x400*regn($a2)+0x100*regn($v);
		$addr+=1;
	    }else{
		# mov [number] reg
		$bin{$addr} = 0x3 + 0x400*regn($a2) + (n($v)<<16);
		$addr+=1;
	    }
	}elsif(isreg($a1) and isvar($a2) ) {
	    # mov reg var ; ref var
	    $bin{$addr} = 0x1 + ($var{$a2}<<16)+0x40 *regn($a1);
	    $addr+=1;
	}else{
	    print "error; wrong mov command '$_'\n";
	    exit(1);
	}
    }
    
    if(/^\s*(add|sub|and|or|xor)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s*$/){
	my $a1 = $2;
	my $a2 = $3;
	my $a3 = $4;
	$bin{$addr} = 0x7 + 0x40*regn($a1)+ 0x100*regn($a2) + 0x400*regn($a3) + 0x1000*alusel($1);
	$addr+=1;
    }

    if(/^\s*not\s+([^\s]+)\s+([^\s]+)\s*$/){
	my $a1 = $1;
	my $a2 = $2;
	$bin{$addr} = 0x7 + 0x40*regn($a1)+ 0x100*regn($a2)  + 0x1000*alusel("not");
	$addr+=1;
    }    
}


#print Dumper(\%label);
#print Dumper(\%var);
#print Dumper(\%bin);


my @l = sort {$a <=> $b} keys %bin;
my $last = $l[$#l];

open(f,">",$fileout);
printf f "v2.0 raw\n";
for(my $i=0;$i<=$last;++$i){
    if( exists $bin{$i} ) { printf f "%x ", $bin{$i}; }else{ print f "0 "; } 
}
close f;



#print Dumper(\@lines);


