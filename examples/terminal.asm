
offset 0x0000


    ;used for comaration
    mov y 0

loop:
    ;reading keyboard buffer flag
    mov x [kflag]
    
    ;comparing if x == 1 or 0
    cmpe x y
    
    ; if x == 0 then no char in keyboard's buffer
    jmpif skip
    
    ; reading character from keyboard's buffer
    mov x [kchar]
    
    ; puting on therminal
    mov [terminal] x
skip:
    
    ; do other things here

    jmp loop






; terminal 
; write on terminal by writing to memory address 0xff00
offset 0xff00
var terminal

;keyboard flag
; if value at address 0xff01 is 1 then there is a char in keyboard buffer
offset 0xff01
var kflag

;keyboard char
; reading from memory 0xff02 reads from keyboard's buffor
offset 0xff02
var kchar

