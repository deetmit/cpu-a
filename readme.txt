CPU A

Log:
30.05.2020 - finished cpu and compiler 


Description.

Simple CPU on Logisim:
- all instructions have 4 bytes
- ROM for code, RAM for data
- ROM addresseed by 4byte chunks
- RAM addressed by 2bytes chunks
- all instructinos executed in 1 clock cicle
- has keyboard, terminal and display 128x128
- as.pl is an asembler compiler


Quick start:
Open cpu file in Logisim. Find ROM module and load one of the *.bin files. Set IP register to 0x000. Switch
between manual or automatic clock. Run the symulation.


Compiling assembler code.
To compile *.asm run:

./as.pl file.asm file.bin

it will crate *.bin file. Use *.bin file to load image in ROM module.



Assembler:
Supports the following instructions:

nop
jmp <label>
jmpif <label>
jmplong <label>
jmplong <address>
cmp <reg> <reg>|<var>
mov <reg> <reg>|<number>
mov <reg> [reg|address|var]
mov [var|address|reg] reg

add|sub|and|or|xor <reg> <reg> <reg>
not <reg> <reg>


where 
    mov r1 r2 means  r1:=r2
    add r1 r2 r3 means r1=r2+r3
    

Registers
X, Y, Z, W (lower case are acceptable)

Labels and vars:
[a-zA-Z_][a-zA-Z_0-9]*

Offset:
You can change current offset by command 

offset value
offset +value
offset -value

eg.
offset 0xff00
offset +0x10


Vars:
Vars are used to access RAM. To set the addres use offset.

eg.
offset 0x10
var A       <- variable of addres 0x10

Numbers:
Standars formats: 0xab, abh, 1930, 010101010b 
    

Instruction format:
For details see asm.txt
0                               15 16                              31
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|   cmd     | i |o1 |o2 |  sel  |  |            arg1               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
            +0x40
                +0x100
                     +0x400
                        +0x1000
 

 